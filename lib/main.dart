import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {


    return MaterialApp(
    title:  "Startup Name and Generate",
    home: RandomWords(),
      theme: ThemeData(
        primaryColor: Colors.purple,
      ),
    );
 }
}
class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();

}
class RandomWordsState extends State<RandomWords>{
  final List <WordPair> _Suggestions=<WordPair>[];
  final Set<WordPair> _saved = Set<WordPair>();   // Add this line.
  final TextStyle _biggerFont = TextStyle(fontSize: 18.0);
  @override
  Widget build(BuildContext Context){
    final _biggerFront= const TextStyle(fontSize: 18.0);

    Widget _buildRow(WordPair pair){
      final bool alreadySaved = _saved.contains(pair);
      return ListTile(
        title: Text(
          pair.asPascalCase,
          style: _biggerFront,
        ),
        trailing: Icon(
          alreadySaved ? Icons.favorite: Icons.favorite_border,
          color: alreadySaved ? Colors.red : null,

      ),
        onTap: (){
          setState((){
            alreadySaved ? _saved.remove(pair) : _saved.add(pair);

          });
        },
      );
    }

    Widget _buildSuggestions(){
      return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context,i){
          if(i.isOdd) return Divider ();
          final index = i~/2;
          if(index >= _Suggestions.length){
            _Suggestions.addAll(generateWordPairs().take(10));
          }
          return _buildRow(_Suggestions[index]);
      }
      );
    }
    void _pushSaved(){
      Navigator.of(context).push(
        MaterialPageRoute <void>(
          builder:(BuildContext context){
            final Iterable<ListTile> tiles= _saved.map(
                (WordPair pair){
                  return ListTile(
                    title: Text(
                      pair.asPascalCase,
                      style: _biggerFont,
                    ),

                  );
                },
            );
            final List<Widget> divided = ListTile
                .divideTiles(
              context: context,
              tiles: tiles,
            )
              .toList();
            return Scaffold(appBar: AppBar(
            title: Text('Saved Suggestions'),
            ),
              body: ListView(children: divided,
              ),
            );
          },
        )
      );

    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Startup Name and Generater"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.list),onPressed: _pushSaved,)
        ],

      ),

      body: _buildSuggestions(),
    );
  }
}
